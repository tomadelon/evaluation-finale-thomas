import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  public username: string = '';
  public password: string = '';

  
  constructor(public userService: UserService, private router: Router) { }

  async onSubmitCreateUser() {
    let user = await this.userService.createUser(this.username, this.password);
    this.router.navigate(['/']);
  }

  ngOnInit(): void {
  }

}
