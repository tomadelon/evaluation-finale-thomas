import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import axios from 'axios';
import { Question } from 'src/app/models/question.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.scss']
})
export class CreateQuestionComponent implements OnInit {

  constructor(public questionService: QuestionService, private router: Router, public authentService: AuthenticationService) { }
 
  question:Question = {
  id:"",
  date:"",
  title:"",
  content:"",
  user: this.authentService.getCurrentUser()

}

  

  onSubmitCreateQuestion() {
     this.questionService.createQuestion(this.question)
    this.router.navigate(['/'])
  }
 

  ngOnInit(): void {

  }

}
