import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnswerService } from 'src/app/services/answer.service';

@Component({
  selector: 'app-create-answer',
  templateUrl: './create-answer.component.html',
  styleUrls: ['./create-answer.component.scss']
})
export class CreateAnswerComponent implements OnInit {

  public id:string = "";
  public content:string = "";

  constructor(public answerService: AnswerService, private router: Router, private route: ActivatedRoute) { }


  async onSubmitCreateAnswer() {
    await this.answerService.createAnswer(this.id, this.content)
    this.router.navigate(['answers/question', this.id])
  }
  

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
  }

}
