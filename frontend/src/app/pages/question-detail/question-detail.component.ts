import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Answer } from 'src/app/models/answer.model';
import { Question } from 'src/app/models/question.model';
import { AnswerService } from 'src/app/services/answer.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-question-detail',
  templateUrl: './question-detail.component.html',
  styleUrls: ['./question-detail.component.scss']
})
export class QuestionDetailComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, public questionService: QuestionService, public answerService: AnswerService, public router: Router, public authentService: AuthenticationService ) { }

  public question?: Question;
  public answer?: Answer;
  public id:string = "";
  public content:string = "";

  async onSubmitCreateAnswer() {
    await this.answerService.createAnswer(this.id, this.content)
    this.router.navigate(['answers/question/:id'])
  }

  async ngOnInit() {
    await this.activatedRoute.params.subscribe(params => {
      this.answerService.getQuestionDetail(params['id']);
      this.questionService.getQuestionById(params['id'])
       .then((question:any)=>{
         this.question=question.data;
      })
    })
  }

}
