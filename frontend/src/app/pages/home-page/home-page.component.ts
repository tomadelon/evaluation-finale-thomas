import { Component, Input, OnInit } from '@angular/core';
import { Question } from 'src/app/models/question.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor(public questionService: QuestionService, public authentService: AuthenticationService) { }
  @Input()
  public question?:Question
  
  // enlever le async await si bug
   async ngOnInit() {

    await this.questionService.fetchQuestions();
  }

}
