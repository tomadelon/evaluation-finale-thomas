
import { Injectable } from '@angular/core';
import axios from 'axios';
import { Question } from '../models/question.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  public questions: Question[] = [];
  public question?: Question;

  constructor() { }

  apiUrl : String ='http://localhost:8080/questions';

  public async fetchQuestions() {
    this.questions = await this.getQuestions();
  }

  public async getQuestions() {
    const response = await axios.get<Question[]>("http://localhost:8080/questions");
    try {
      return response.data;
    } catch (error) {
      console.log(error);
    }
    return []
  }

  public fetchQuestionsByTitle(title: string) {
    axios.get(`${this.apiUrl}`, {
      params: {
        title: title,
      }
    })
      .then((res) => {
        console.log(res.data);
        this.questions = res.data;
      })
      .catch((err => {
        console.log(err);
      }))
  }

  public createQuestion(question:Question) {
    return axios.post(`${this.apiUrl}`, {
      id:question.id,
      date:question.date,
      title:question.title,
      content:question.content,
      user:question.user
    
    })
      .then(function (response) {
        console.log(response.data);
        return response.data
      })
      .catch(function (error) {
        console.log(error);
        return error;
      });
  }

  // public createQuestion(title: string, content: string): Promise<Question> {
  //   return axios.post(`${this.apiUrl}`,
  //     {
  //       title: title,
  //       content: content,
  //     })
  // }


  public getQuestionById(id: string): Promise<Question> {
    return axios.get(`${this.apiUrl}/id/${id}`)
      
  }

}

  



