import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import axios from 'axios';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private readonly SESSION_STORAGE_KEY = 'currentUser';
  apiUrl: String = 'http://localhost:8080/users';
  
  public user?: User;

  constructor(private route: Router) {}

  verify(): Promise<any> {
    return axios.post(`${this.apiUrl}/me`);
  }

  isLogged(): boolean {
    if (sessionStorage.getItem(this.SESSION_STORAGE_KEY) ) {
      return true
    }
    else{
      return false
    }
  }

  login(user: User): void {
    sessionStorage.setItem(this.SESSION_STORAGE_KEY, JSON.stringify(user));
    sessionStorage.setItem('isLogged', 'true');
  }

  logout(): void {
    sessionStorage.removeItem(this.SESSION_STORAGE_KEY);
    sessionStorage.setItem('isLogged', 'false');
    this.route.navigate(['login'])
  }


  getCurrentUserBasicAuthentication(): string {
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY);
    console.log(currentUserPlain);
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain);
      console.log(currentUser);
      return 'Basic ' + btoa(currentUser.username + ':' + currentUser.password);
    } else {
      return '';
    }
  }

  getCurrentUser(): any {
    const currentUserPlain = sessionStorage.getItem('session')
    //const user?:User;
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)
      return {username : currentUser.username,
        password : currentUser.password};
  }
}

}
