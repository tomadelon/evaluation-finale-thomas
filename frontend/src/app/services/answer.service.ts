import { Injectable } from '@angular/core';
import axios from 'axios';
import { Answer } from '../models/answer.model';


@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  apiUrl : String ='http://localhost:8080/answers';
  answers:Answer[] = []
  
  constructor() { }

  public getQuestionDetail(id: string) {
    axios.get(`${this.apiUrl}/question/${id}`)
      .then((res) => {
        console.log(res.data);
        this.answers = res.data;
      })
      .catch((err => {
        console.log(err);
      }))
  }

  public createAnswer(id: string, content:string): Promise<Answer> {

    return axios.post(`${this.apiUrl}/question/${id}`, { content: content})
      
  }

  

}
