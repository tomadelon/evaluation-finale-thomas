import { Router } from "@angular/router";
import axios from "axios";
import { AuthenticationService } from "../services/authentication.service";


export const errorInterceptor = (authentService:AuthenticationService, router: Router): void => {
    axios.interceptors.response.use(response => {
        return response;
      }, error => {
        if (error.response.status === 401) {
            authentService.logout();
              router.navigate(['/login']);
        }
        throw error;

    })
}