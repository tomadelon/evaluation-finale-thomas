import axios, { AxiosError, AxiosRequestConfig } from "axios"
import { AuthenticationService } from "../services/authentication.service"


export const authInterceptor = (authentService: AuthenticationService): void => {
    
     axios.interceptors.request.use(
       (config: AxiosRequestConfig) => {
         if(config.headers &&config.method!=='get')
         config.headers['Authorization'] = authentService.getCurrentUserBasicAuthentication()
         return config
       },
       (error: AxiosError) => {
         console.error('ERROR:', error)
         Promise.reject(error)
       })

      
    }

