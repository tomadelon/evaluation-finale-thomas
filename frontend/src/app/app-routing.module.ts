import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateAnswerComponent } from './pages/create-answer/create-answer.component';
import { CreateQuestionComponent } from './pages/create-question/create-question.component';
import { CreateUserComponent } from './pages/create-user/create-user.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginComponent } from './pages/login/login.component';
import { QuestionDetailComponent } from './pages/question-detail/question-detail.component';

const routes: Routes = [
  { path: '', component: HomePageComponent},
  { path: 'answers/question/:id', component: QuestionDetailComponent},
  { path: 'questions/create', component: CreateQuestionComponent},
  { path: 'answers/create/:id', component: CreateAnswerComponent},
  { path: 'login', component: LoginComponent},
  { path: 'user', component: CreateUserComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
