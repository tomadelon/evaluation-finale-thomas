import { User } from "./user.model";

export interface Question {
    id: string,
    date: string,
    title: string,
    content: string,
    user: User,
}