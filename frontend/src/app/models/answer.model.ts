import { Question } from "./question.model";
import { User } from "./user.model";

export interface Answer {
    id:string,
    answerDate: Date,
    content: string,
    question: Question,
    user: User
}