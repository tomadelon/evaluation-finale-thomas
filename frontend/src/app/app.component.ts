import { Component } from '@angular/core';
import { authInterceptor } from './helpers/interceptor';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';

  constructor(private authentService: AuthenticationService) {
    authInterceptor(this.authentService);
  }
}
