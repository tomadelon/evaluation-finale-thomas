import { Component, Input, OnInit } from '@angular/core';
import { Answer } from 'src/app/models/answer.model';
import { AnswerService } from 'src/app/services/answer.service';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.scss']
})
export class AnswerComponent implements OnInit {

  constructor() { }

  @Input()
  public answer?: Answer;

  ngOnInit(): void {
  }

}
