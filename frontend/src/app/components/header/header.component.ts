import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public search: string = '';

  constructor(public authentService: AuthenticationService, private questionService: QuestionService) { }

  onKeyupSearch(): void {
    this.questionService.fetchQuestionsByTitle(this.search);
  }

  ngOnInit(): void {
  }

}
