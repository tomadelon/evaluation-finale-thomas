package com.zenika.barbajavas.evaluationfinalepoei.application;

import com.zenika.barbajavas.evaluationfinalepoei.domain.model.question.BadLengthTitleException;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.question.Question;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.users.User;
import com.zenika.barbajavas.evaluationfinalepoei.domain.repository.QuestionRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
@ActiveProfiles("test")
public class QuestionServiceTests {

    @MockBean
    UserService userService;

    @MockBean
    QuestionRepository questionRepository;

    @Autowired
    QuestionService questionService;

    @Test
    @WithMockUser(username = "admin")
    void testCreateQuestionOk() throws RuntimeException, BadLengthTitleException {
        //Mock UserService
        Mockito.when(userService.getUserByUsername("admin")).thenReturn
                (Optional.of(new User("380eba2b-a920-4a11-b36b-8c613370609f","admin","$2a$10$aJmcH58nAN5Tc7fQcmxwGudpxaP3EXZKERQHMOWq5mG2oRUGsvUDG")));
        Question question = new Question("1234", LocalDateTime.now().withNano(0), "question?", "quelle est la question svp", null);

        Mockito.when(questionRepository.save(any(Question.class))).thenReturn(question);

        Question createdQuestion = questionService.createQuestion(question.getTitle(), question.getContent());

        assertEquals(createdQuestion.getId(),question.getId());
    }


}
