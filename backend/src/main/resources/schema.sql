create extension if not exists pg_trgm;

create table if not exists users
(
    id char(36) primary key,
    username text not null unique,
    password text not null
);

create table if not exists questions
(
    id char(36) primary key,
    question_date TIMESTAMP not null,
    title text not null unique,
    content text not null,
    question_user_id char(36) REFERENCES users(id)

);

create table if not exists answers
(
    id char(36) primary key,
    answer_date TIMESTAMP not null,
    content text not null,
    question_id char(36) REFERENCES questions(id),
    answer_user_id char(36) REFERENCES users(id)

);

