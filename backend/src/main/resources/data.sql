create table if not exists users
(
    id char(36) primary key,
    username text not null unique,
    password text not null
);

create table if not exists questions
(
    id char(36) primary key,
    question_date TIMESTAMP not null,
    title text not null unique,
    content text not null,
    question_user_id char(36) REFERENCES users(id)

);

create table if not exists answers
(
    id char(36) primary key,
    answer_date TIMESTAMP not null,
    content text not null,
    question_id char(36) REFERENCES questions(id),
    answer_user_id char(36) REFERENCES users(id)

);


insert into users values ('380eba2b-a920-4a11-b36b-8c613370609f', 'admin', '$2a$10$aJmcH58nAN5Tc7fQcmxwGudpxaP3EXZKERQHMOWq5mG2oRUGsvUDG') on conflict do nothing;
insert into users values ('059bb375-491e-4073-91d1-92b4c5e332bc', 'thomas', '$2a$10$MrmQsTWM8h67dvQ74UaEIuWiXW9EYemzxPZg.8QnPsSNddKy7JnL.') on conflict do nothing;
insert into users values ('8f943c90-fcd1-43d6-93fb-14abb62cc203', 'vous', '$2a$10$spp5uqRrUAmYa4meXnMZuOEcIGNheog46HDuGjoZnfK7B.RtqQuhW') on conflict do nothing;

insert into questions values ('66e965a4-35ee-4b58-b00d-b2a01c68ee8b', '2022-05-05 16:17:32', 'Comment donner plus de visibilité aux femmes en entreprise ?', 'Nous connaissons tous·tes les inégalités femmes-hommes dans le monde professionnel. Un groupe de femmes chez Zenika a décidé de faire bouger les lignes. Depuis 1 mois, elles ont lancé la phase pilote d’un tout nouveau parcours interne de développement personnel et professionnel dédié aux femmes : le Parcours Women Empowerment. D’où est née l’idée ? Comment ce parcours a-t-il été construit ? Et comment organiser et mettre en place ce type de parcours ?', '059bb375-491e-4073-91d1-92b4c5e332bc') on conflict do nothing;
insert into questions values ('f532c1f6-c790-4c20-be2f-11340bc25cb3', '2022-05-05 16:44:18', 'Mot de passe et question secrète oublié comment récupérer ?', 'Bonjour, Je veux récupérer les données du coffre fort alors que jai oublié le mot de passe et question secrète svp jai vraiment besoin de les récupérer', '8f943c90-fcd1-43d6-93fb-14abb62cc203') on conflict do nothing;

insert into answers values ('e3c29179-e98c-4516-9665-3d807947c7c2', '2022-05-05 17:59:16', 'Peut- être serait-il judicieux de demander à un administrateur du site en question? bon courage à vous en tout cas :)', 'f532c1f6-c790-4c20-be2f-11340bc25cb3', '380eba2b-a920-4a11-b36b-8c613370609f') on conflict do nothing;
insert into answers values ('41701a32-c66d-435b-b6ad-184fa4dffe32', '2022-05-05 18:04:17', 'Merci bcp !! Effectivement, cest ce que jai fait et ils ont été super sympas... ils mont renvoyé un formulaire pour changer mon mot de passe !!!!', 'f532c1f6-c790-4c20-be2f-11340bc25cb3', '8f943c90-fcd1-43d6-93fb-14abb62cc203') on conflict do nothing;