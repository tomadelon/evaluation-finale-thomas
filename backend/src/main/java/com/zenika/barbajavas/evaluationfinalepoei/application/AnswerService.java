package com.zenika.barbajavas.evaluationfinalepoei.application;

import com.zenika.barbajavas.evaluationfinalepoei.domain.model.answer.Answer;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.answer.BadLengthContentException;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.question.Question;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.users.User;
import com.zenika.barbajavas.evaluationfinalepoei.domain.repository.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AnswerService {

    private final AnswerRepository answerRepository;
    private final QuestionService questionService;
    private final UserService userService;

    @Autowired
    public AnswerService(AnswerRepository answerRepository, QuestionService questionService, UserService userService) {
        this.answerRepository = answerRepository;
        this.questionService = questionService;
        this.userService = userService;
    }

    public Answer createAnswer(String content, String questionId) throws BadLengthContentException {
        String[] words = content.split(" ");
        int numberOfWords = words.length;
        if (numberOfWords <= 100) {
            Optional<Question> questionById = questionService.getQuestionById(questionId);
            Question question = questionById.orElseThrow(RuntimeException::new);
            Optional <User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            User user = userByUsername.orElseThrow(RuntimeException::new);

            Answer answer = new Answer((UUID.randomUUID().toString()), LocalDateTime.now().withNano(0), content, question, user);
            answer = answerRepository.save(answer);
            return answer;
        } else {
            throw new BadLengthContentException();
        }
    }

    public List<Answer> findQuestionAnswers(String questionId) {
        return answerRepository.findByQuestionIdOrderByAnswerDateDesc(questionId);
    }
}
