package com.zenika.barbajavas.evaluationfinalepoei.web.question;

import java.time.LocalDateTime;

public class CreatedQuestionDto {

    private String id;
    private LocalDateTime date;
    private String title;
    private String content;

    public CreatedQuestionDto(String id, LocalDateTime date, String title, String content) {
        this.id = id;
        this.date = date;
        this.title = title;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
