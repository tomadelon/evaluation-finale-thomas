package com.zenika.barbajavas.evaluationfinalepoei.application;

import com.zenika.barbajavas.evaluationfinalepoei.domain.model.users.User;
import com.zenika.barbajavas.evaluationfinalepoei.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class UserService {

    private final UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User createUser(String username, String password){

        User user = new User(UUID.randomUUID().toString(), username, password);
        user.setPassword(passwordEncoder.encode(password));

        return userRepository.save(user);
    }
    public Optional<User> getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

}
