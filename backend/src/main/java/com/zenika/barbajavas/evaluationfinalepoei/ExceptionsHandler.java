package com.zenika.barbajavas.evaluationfinalepoei;

import com.zenika.barbajavas.evaluationfinalepoei.domain.model.answer.BadLengthContentException;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.question.BadLengthTitleException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler(value = BadLengthTitleException.class)
    public final ResponseEntity<ApiError> badLengthTitleException(BadLengthTitleException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Le titre ne doit pas dépasser 20 mots"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = BadLengthContentException.class)
    public final ResponseEntity<ApiError> badLengthContentException(BadLengthContentException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Le contenu ne doit pas dépasser 100 mots"), HttpStatus.BAD_REQUEST);
    }

}
