package com.zenika.barbajavas.evaluationfinalepoei.application;

import com.zenika.barbajavas.evaluationfinalepoei.domain.model.question.BadLengthTitleException;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.question.Question;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.users.User;
import com.zenika.barbajavas.evaluationfinalepoei.domain.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class QuestionService {

    private final QuestionRepository questionRepository;
    private final UserService userService;

    @Autowired
    public QuestionService(QuestionRepository questionRepository, UserService userService) {
        this.questionRepository = questionRepository;
        this.userService = userService;
    }

    public Question createQuestion(String title, String content) throws BadLengthTitleException {
        String[] words = title.split(" ");
        int numberOfWords = words.length;
        if (numberOfWords <= 20 && (words[numberOfWords -1].contains("?"))) {
            Optional<User> userByUsername =
                    userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            User user = userByUsername.orElseThrow(RuntimeException::new);
            Question question = new Question((UUID.randomUUID().toString()), LocalDateTime.now().withNano(0), title, content, userByUsername.orElse(null));
            return questionRepository.save(question);
        } else {
            throw new BadLengthTitleException();
        }
    }

    public Optional<Question> getQuestionById(String id) {
        return this.questionRepository.findById(id);
    }

    public Iterable<Question> getAllQuestionByTitle(String title) {
        return questionRepository.findByTitle(title);
    }

    public Iterable<Question> getAllQuestions() {
        return this.questionRepository.findByOrderByQuestionDateDesc();
    }
}
