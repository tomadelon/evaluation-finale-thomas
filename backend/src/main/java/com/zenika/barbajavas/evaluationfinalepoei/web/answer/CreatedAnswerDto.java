package com.zenika.barbajavas.evaluationfinalepoei.web.answer;

import java.time.LocalDateTime;

public class CreatedAnswerDto {

    private String Id;
    private LocalDateTime date;
    private String content;

    public CreatedAnswerDto(String id, LocalDateTime date, String content) {
        Id = id;
        this.date = date;
        this.content = content;
    }

    public String getId() {
        return Id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
