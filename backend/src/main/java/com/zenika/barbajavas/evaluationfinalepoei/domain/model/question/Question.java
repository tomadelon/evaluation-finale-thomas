package com.zenika.barbajavas.evaluationfinalepoei.domain.model.question;


import com.zenika.barbajavas.evaluationfinalepoei.domain.model.users.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "questions")
@Access(AccessType.FIELD)
public class Question {

    @Id
    private String id;
    protected LocalDateTime questionDate;
    private String title;
    private String content;
    @ManyToOne
    @JoinColumn(name=("question_user_id"))
    private User user;



    protected Question() {
        //FOR JPA
    }

    public Question(String id, LocalDateTime questionDate, String title, String content, User user) {
        this.id = id;
        this.questionDate = questionDate;
        this.title = title;
        this.content = content;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public LocalDateTime getQuestionDate() {
        return questionDate;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public User getUser() {
        return user;
    }
}
