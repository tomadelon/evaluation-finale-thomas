package com.zenika.barbajavas.evaluationfinalepoei.web.users;

public record UserDto(String username, String password) {
}
