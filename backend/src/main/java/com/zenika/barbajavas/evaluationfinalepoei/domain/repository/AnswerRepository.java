package com.zenika.barbajavas.evaluationfinalepoei.domain.repository;

import com.zenika.barbajavas.evaluationfinalepoei.domain.model.answer.Answer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AnswerRepository extends CrudRepository<Answer, String> {

    public List<Answer> findByQuestionIdOrderByAnswerDateDesc(String questionId);
}
