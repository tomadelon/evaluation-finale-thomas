package com.zenika.barbajavas.evaluationfinalepoei.domain.repository;

import com.zenika.barbajavas.evaluationfinalepoei.domain.model.users.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {

    public Optional<User> findByUsername(String username) ;
}
