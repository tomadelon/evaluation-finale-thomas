package com.zenika.barbajavas.evaluationfinalepoei.domain.model.answer;

import com.zenika.barbajavas.evaluationfinalepoei.domain.model.question.Question;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.users.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="answers")
@Access(AccessType.FIELD)
public class Answer {

    @Id
    private String id;
    private LocalDateTime answerDate;
    private String content;


    @ManyToOne
    @JoinColumn(name=("question_id"))
    private Question question;

    @ManyToOne
    @JoinColumn(name=("answer_user_id"))
    private User user;

    protected Answer() {
        //FOR JPA
    }

    public Answer(String id, LocalDateTime answerDate, String content, Question question, User user) {
        this.id = id;
        this.answerDate = answerDate;
        this.content = content;
        this.question = question;
        this.user= user;
    }

    public String getId() {
        return id;
    }

    public LocalDateTime getAnswerDate() {
        return answerDate;
    }

    public String getContent() {
        return content;
    }

    public Question getQuestion() {
        return question;
    }

    public User getUser() {
        return user;
    }
}
