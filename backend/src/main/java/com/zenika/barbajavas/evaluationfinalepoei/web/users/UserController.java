package com.zenika.barbajavas.evaluationfinalepoei.web.users;

import com.zenika.barbajavas.evaluationfinalepoei.application.UserService;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;


    public UserController(UserService userService) {
        this.userService = userService;
    }


    @PostMapping("/me")
    @ResponseStatus(value = HttpStatus.OK)
    public String me() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @PostMapping("")
    @ResponseStatus(value = HttpStatus.CREATED)
    ResponseEntity<User> createUser(@RequestBody UserDto body) {
        User createdUser = userService.createUser(body.username(), body.password() );
        return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
    }



}
