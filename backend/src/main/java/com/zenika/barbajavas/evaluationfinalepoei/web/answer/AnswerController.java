package com.zenika.barbajavas.evaluationfinalepoei.web.answer;

import com.zenika.barbajavas.evaluationfinalepoei.application.AnswerService;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.answer.Answer;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.answer.BadLengthContentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/answers")
public class AnswerController {

    private final AnswerService answerService;


    @Autowired
    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @PostMapping("/question/{questionId}")
    @ResponseStatus(value = HttpStatus.CREATED)
    public Answer createAnswer(@PathVariable String questionId, @RequestBody CreateAnswerDto createAnswerDto) throws BadLengthContentException {
        return this.answerService.createAnswer(createAnswerDto.getContent(),questionId );
    }

    @GetMapping("/question/{questionId}")
    @ResponseStatus(value = HttpStatus.OK)
    List<Answer> findAnswerByQuestion(@PathVariable String questionId) {
        return this.answerService.findQuestionAnswers(questionId);
    }
}
