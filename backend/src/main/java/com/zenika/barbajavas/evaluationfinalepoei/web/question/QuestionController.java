package com.zenika.barbajavas.evaluationfinalepoei.web.question;

import com.zenika.barbajavas.evaluationfinalepoei.application.QuestionService;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.question.BadLengthTitleException;
import com.zenika.barbajavas.evaluationfinalepoei.domain.model.question.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/questions")
public class QuestionController {

    private final QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public Question createQuestion(@RequestBody CreateQuestionDto createQuestionDto) throws BadLengthTitleException {
        return this.questionService.createQuestion(createQuestionDto.getTitle(), createQuestionDto.getContent());
    }

    @GetMapping
    @ResponseStatus(value = HttpStatus.OK)
    public Iterable<CreatedQuestionDto> getAllListedQuestions(@RequestParam(name = "title", required = false) String title) {
        Iterable<Question> allQuestions = Collections.emptyList();
        if (title != null) {
            allQuestions = this.questionService.getAllQuestionByTitle(title);
        } else {
            allQuestions = this.questionService.getAllQuestions();
        }
          List<CreatedQuestionDto> questionsDto = new ArrayList<>();
          for (Question question : allQuestions) {
            questionsDto.add(new CreatedQuestionDto(question.getId(), question.getQuestionDate(), question.getTitle(), question.getContent()));
        }
        return questionsDto;
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Question> findQuestionById(@PathVariable String id) {
        return questionService.getQuestionById(id).map(ResponseEntity::ok).orElseThrow();
    }
}
