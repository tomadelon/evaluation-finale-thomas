package com.zenika.barbajavas.evaluationfinalepoei.web.answer;

public class CreateAnswerDto {

    private String content;

    protected CreateAnswerDto() {}

    public CreateAnswerDto(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
