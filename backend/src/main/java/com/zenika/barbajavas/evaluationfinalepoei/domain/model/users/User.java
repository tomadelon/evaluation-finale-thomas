package com.zenika.barbajavas.evaluationfinalepoei.domain.model.users;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;

@Entity
@Table(name = "users")
@Access(AccessType.FIELD)
public class User implements UserDetails {

    @Id
    private String id;
    private String username;
    private String password;

    protected User() {
        // For JPA
    }

    public User(String id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password= password;
    }

    public String getId() {
        return id;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    /**public void setUsername(String username) {
     if(!StringUtils.hasText(username)) {
     throw new IllegalArgumentException("Username can't be null");
     }
     this.username = username;
     }*/

    public void setPassword(String password) {
        this.password = password;
    }
}
